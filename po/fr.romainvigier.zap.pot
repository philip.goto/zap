# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.zap package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.zap\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-06 16:34+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Application name, avoid translating it!
#: data/fr.romainvigier.zap.desktop:8 data/fr.romainvigier.zap.metainfo.xml:10
#: data/ui/Window.ui:168 src/classes/Application.js:56
msgid "Zap"
msgstr ""

#. Translators: Generic application name.
#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.desktop:10 data/fr.romainvigier.zap.metainfo.xml:43
msgid "Soundboard"
msgstr ""

#. Translators: Short description of the application.
#: data/fr.romainvigier.zap.desktop:12 data/fr.romainvigier.zap.metainfo.xml:22
msgid "Play sounds from a soundboard"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/fr.romainvigier.zap.desktop:19
msgid "Audio;Effect;Player;Sound;Soundboard;Streaming;"
msgstr ""

#: data/fr.romainvigier.zap.metainfo.xml:25
msgid ""
"Play all your favorite sound effects! This handy soundboard makes your "
"livestreams and videocasts more entertaining."
msgstr ""

#: data/fr.romainvigier.zap.metainfo.xml:26
msgid ""
"Import audio files, arrange them in collections and customize their "
"appearance."
msgstr ""

#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.metainfo.xml:35
msgid "Audio"
msgstr ""

#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.metainfo.xml:37
msgid "Effect"
msgstr ""

#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.metainfo.xml:39
msgid "Player"
msgstr ""

#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.metainfo.xml:41
msgid "Sound"
msgstr ""

#. Translators: Search term to find this application.
#: data/fr.romainvigier.zap.metainfo.xml:45
msgid "Streaming"
msgstr ""

#: data/ui/AddZapPopup.ui:28 data/ui/Window.ui:137
msgid "Add a Zap"
msgstr ""

#: data/ui/AddZapPopup.ui:36
msgid "Close"
msgstr ""

#: data/ui/AddZapPopup.ui:65 data/ui/CollectionItem.ui:122
#: data/ui/CollectionsMenuButton.ui:97 data/ui/EditZapPopover.ui:25
msgid "Name"
msgstr ""

#: data/ui/AddZapPopup.ui:67
msgid "Clear"
msgstr ""

#: data/ui/AddZapPopup.ui:76
msgid "Add"
msgstr ""

#: data/ui/CollectionItem.ui:64
msgid "Playing"
msgstr ""

#: data/ui/CollectionItem.ui:89 data/ui/ZapItem.ui:126
msgid "Edit"
msgstr ""

#: data/ui/CollectionItem.ui:112 data/ui/EditZapPopover.ui:107
msgid "Remove"
msgstr ""

#: data/ui/CollectionItem.ui:129
msgid "Done"
msgstr ""

#: data/ui/CollectionsMenuButton.ui:104
msgid "Add collection"
msgstr ""

#: data/ui/EditZapPopover.ui:31
msgid "Collection"
msgstr ""

#: data/ui/EditZapPopover.ui:85
msgid "Volume"
msgstr ""

#: data/ui/FileChooserButton.ui:30
msgid "Select file…"
msgstr ""

#: data/ui/FileChooserButton.ui:71
msgid "Choose an audio file"
msgstr ""

#: data/ui/FileChooserButton.ui:78
msgid "Audio files"
msgstr ""

#: data/ui/ShortcutsWindow.ui:15
msgid "Zap Management"
msgstr ""

#: data/ui/ShortcutsWindow.ui:18
msgid "Open Add Zap Popup"
msgstr ""

#: data/ui/ShortcutsWindow.ui:24
msgid "Open Collections Popover"
msgstr ""

#: data/ui/ShortcutsWindow.ui:32
msgid "General"
msgstr ""

#: data/ui/ShortcutsWindow.ui:35 data/ui/Window.ui:189
msgid "New Window"
msgstr ""

#: data/ui/ShortcutsWindow.ui:41
msgid "Close Window"
msgstr ""

#: data/ui/ShortcutsWindow.ui:47
msgid "Quit Application"
msgstr ""

#: data/ui/Window.ui:26
msgid "Main Menu"
msgstr ""

#: data/ui/Window.ui:62
msgid "Add Zaps to this collection by clicking the “+” button."
msgstr ""

#. Translators: Replace `translator-credits` by your name, and optionally add your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://www.example.org/`). If names are already present, do not remove them and add yours on a new line, in alphabetical order.
#: data/ui/Window.ui:177
msgid "translator-credits"
msgstr ""

#: data/ui/Window.ui:195
msgid "Keyboard Shortcuts"
msgstr ""

#: data/ui/Window.ui:199
msgid "About Zap"
msgstr ""

#: data/ui/ZapItem.ui:51
msgid "Stop"
msgstr ""

#: data/ui/ZapItem.ui:61
msgid "Fade out"
msgstr ""

#: data/ui/ZapItem.ui:75
msgid "Play"
msgstr ""

#: data/ui/ZapItem.ui:112
msgid "Loop"
msgstr ""

#. Translators: Default collection name.
#: src/data/Collections.js:121
msgid "Zaps"
msgstr ""

#: src/enums/Color.js:24
msgid "Gray"
msgstr ""

#: src/enums/Color.js:32
msgid "Red"
msgstr ""

#: src/enums/Color.js:40
msgid "Orange"
msgstr ""

#: src/enums/Color.js:48
msgid "Green"
msgstr ""

#: src/enums/Color.js:56
msgid "Blue"
msgstr ""

#: src/enums/Color.js:64
msgid "Purple"
msgstr ""
