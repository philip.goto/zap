// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import Tracker from 'gi://Tracker';


const DATABASE_PATH = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), pkg.name, 'db']));
const ONTOLOGY_PATH = Gio.File.new_for_path(GLib.build_filenamev([pkg.pkgdatadir, 'ontology']));


/**
 * Handles the database connection and queries.
 *
 * You need to call the `connect()` method after constructing and the `disconnect()` method before exiting.
 */
export class Database {

    #connection = null;
    #cancellable;

    /** */
    constructor() {
        this.#cancellable = new Gio.Cancellable();
    }

    /**
     * Connect to the database.
     */
    connect() {
        this.#connection = Tracker.SparqlConnection.new(
            Tracker.SparqlConnectionFlags.NONE,
            DATABASE_PATH,
            ONTOLOGY_PATH,
            this.#cancellable
        );
    }

    /**
     * Disconnect from the database.
     */
    disconnect() {
        this.#cancellable.cancel();
        if (!this.#connection)
            return;
        this.#connection.close();
        this.#connection = null;
    }

    /**
     * Batch resources.
     *
     * @param {Tracker.Resource[]} resources Array of resources.
     */
    batch(resources = []) {
        const batch = this.#connection.create_batch();
        resources.forEach(resource => batch.add_resource(null, resource));
        batch.execute(this.#cancellable);
    }

    /**
     * Send a SPARQL query to the database.
     *
     * @param {string} sparql SPARQL query.
     * @returns {Tracker.SparqlCursor} The result cursor.
     */
    query(sparql) {
        return this.#connection.query_statement(sparql, this.#cancellable).execute(this.#cancellable);
    }

    /**
     * Send a SPARQL update query to the database.
     *
     * @param {string} sparql SPARQL update query.
     */
    update(sparql) {
        this.#connection.update(sparql, this.#cancellable);
    }

}
