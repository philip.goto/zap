// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Tracker from 'gi://Tracker';

import { Collection } from '../classes/Collection.js';


/**
 * The Collection Manager handles collection creation, modification and deletion. It saves all the changes to the database.
 *
 * You need to call the `restore()` method after constructing it.
 *
 * It will emit the `collection-added`, `collection-removed` and `collection-updated` signals when a collection is added, removed or updated. The signal parameter is the UUID of the collection.
 */
export class CollectionManager extends GObject.Object {

    /** @type {Gio.ListStore<Collection>} */
    #collections;

    static {
        GObject.registerClass({
            GTypeName: 'ZapCollectionManager',
            Signals: {
                'collection-added': { param_types: [GObject.TYPE_STRING] },
                'collection-removed': { param_types: [GObject.TYPE_STRING] },
                'collection-updated': { param_types: [GObject.TYPE_STRING] },
            },
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     */
    constructor({ ...params } = {}) {
        super(params);
        this.#collections = Gio.ListStore.new(Collection);
    }

    /**
     * A `Gio.ListStore` containing all the collections.
     *
     * @returns {Gio.ListStore<Collection>} The store.
     */
    get collections() {
        return this.#collections;
    }

    /**
     * Restore the collections from the database to the store.
     */
    restore() {
        const cursor = globalThis.database.query(
            `SELECT ?uuid ?name {
                ?collection a zap:Collection;
                    zap:uuid ?uuid;
                    zap:name ?name.
            }`
        );
        while (cursor.next(null)) {
            const data = {};
            for (let i = 0; i < cursor.nColumns; i++) {
                switch (cursor.get_variable_name(i)) {
                    case 'uuid':
                        [data.uuid] = cursor.get_string(i);
                        break;
                    case 'name':
                        [data.name] = cursor.get_string(i);
                        break;
                    default:
                }
            }
            this.#collections.append(new Collection(data));
        }
        if (this.#collections.get_n_items() === 0)
            this.addDefaultCollection();
    }

    /**
     * Retrieve a collection from a UUID.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the collection.
     * @returns {Collection} The retrieved collection.
     */
    get({ uuid }) {
        return this.#findCollection(uuid).collection;
    }

    /**
     * Add a collection.
     *
     * @param {object} params Parameter object.
     * @param {string} params.name Name of the collection.
     * @returns {Collection} The added collection.
     */
    add({ name }) {
        const collection = new Collection({
            uuid: GLib.uuid_string_random(),
            name,
        });
        const resource = Tracker.Resource.new(null);
        resource.set_uri('rdf:type', 'zap:Collection');
        resource.set_string('zap:uuid', collection.uuid);
        resource.set_string('zap:name', collection.name);
        globalThis.database.batch([resource]);
        this.#collections.append(collection);
        this.emit('collection-added', collection.uuid);
        return collection;
    }

    /**
     * Add the default collection.
     */
    addDefaultCollection() {
        /* Translators: Default collection name. */
        const defaultName = _('Zaps');
        this.add({ name: defaultName });
    }

    /**
     * Remove a collection.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the collection.
     */
    remove({ uuid }) {
        const { position } = this.#findCollection(uuid);
        globalThis.database.update(
            `DELETE {
                ?collection a rdfs:Resource
            } WHERE {
                ?collection a zap:Collection;
                    zap:uuid ?uuid.
                FILTER (?uuid = "${Tracker.sparql_escape_string(uuid)}")
            }`
        );
        this.#collections.remove(position);
        this.emit('collection-removed', uuid);
        if (this.#collections.get_n_items() === 0)
            this.addDefaultCollection();
    }

    /**
     * Rename a collection.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the collection.
     * @param {string} params.name New name.
     */
    rename({ uuid, name }) {
        const { collection, position } = this.#findCollection(uuid);
        if (collection.name === name)
            return;
        globalThis.database.update(
            `DELETE {
                ?collection zap:name ?name
            } INSERT {
                ?collection zap:name "${Tracker.sparql_escape_string(name)}"
            } WHERE {
                ?collection a zap:Collection;
                    zap:uuid ?uuid;
                    zap:name ?name.
                FILTER (?uuid = "${Tracker.sparql_escape_string(uuid)}")
            }`
        );
        collection.name = name;
        this.emit('collection-updated', collection.uuid);
        this.#collections.emit('items-changed', position, 1, 1);
    }

    /**
     * @typedef {object} FindCollectionResult
     * @property {Collection} collection The found collection.
     * @property {number} position Position in the store, an unsigned integer.
     */
    /**
     * Convenience function to find a collection in the store.
     *
     * @param {string} uuid UUID to search.
     * @returns {FindCollectionResult} Object containing the collection and its position in the store.
     * @throws Throws an error if no collection is found.
     */
    #findCollection(uuid) {
        for (let i = 0; i < this.#collections.get_n_items(); i++) {
            const collection = this.#collections.get_item(i);
            if (collection.uuid === uuid) {
                return {
                    collection,
                    position: i,
                };
            }
        }
        throw new Error(`No collection with UUID ${uuid}`);
    }

}
