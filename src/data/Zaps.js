// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Tracker from 'gi://Tracker';

import { Color } from '../enums/Color.js';
import { Zap } from '../classes/Zap.js';


const ZAPS_DIR = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), pkg.name, 'zaps']));

/**
 * The Zap Manager handles Zap creation, modification and deletion. It saves all the changes to the database.
 *
 * You need to call the `restore()` method after constructing it.
 *
 * It will emit the `zap-added`, `zap-removed` and `zap-updated` signals when a Zap is added, removed or updated. The signal parameter is the UUID of the Zap.
 */
export class ZapManager extends GObject.Object {

    /** @type {Gio.ListStore<Zap>} */
    #zaps;

    static {
        GObject.registerClass({
            GTypeName: 'ZapZapManager',
            Signals: {
                'zap-added': { param_types: [GObject.TYPE_STRING] },
                'zap-removed': { param_types: [GObject.TYPE_STRING] },
                'zap-updated': { param_types: [GObject.TYPE_STRING] },
            },
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     */
    constructor({ ...params } = {}) {
        super(params);
        this.#zaps = Gio.ListStore.new(Zap);
        if (!ZAPS_DIR.query_exists(null))
            ZAPS_DIR.make_directory_with_parents(null);
    }

    /**
     * A `Gio.ListStore` containing all the Zaps.
     *
     * @returns {Gio.ListStore<Zap>} The store.
     */
    get zaps() {
        return this.#zaps;
    }

    /**
     * Restore the Zaps from the database to the store.
     */
    restore() {
        const cursor = globalThis.database.query(
            `SELECT ?uuid ?name ?collectionUuid ?uri ?color ?loop ?volume ?position {
                ?zap a zap:Zap;
                    zap:uuid ?uuid;
                    zap:name ?name;
                    zap:collectionUuid ?collectionUuid;
                    zap:uri ?uri;
                    zap:color ?color;
                    zap:loop ?loop;
                    zap:volume ?volume;
                    zap:position ?position.
            }`
        );
        while (cursor.next(null)) {
            const data = {};
            for (let i = 0; i < cursor.nColumns; i++) {
                switch (cursor.get_variable_name(i)) {
                    case 'uuid':
                        [data.uuid] = cursor.get_string(i);
                        break;
                    case 'name':
                        [data.name] = cursor.get_string(i);
                        break;
                    case 'collectionUuid':
                        [data.collectionUuid] = cursor.get_string(i);
                        break;
                    case 'uri':
                        data.file = Gio.File.new_for_uri(cursor.get_string(i)[0]);
                        break;
                    case 'color':
                        data.color = Color.fromId(cursor.get_string(i)[0]);
                        break;
                    case 'loop':
                        data.loop = cursor.get_boolean(i);
                        break;
                    case 'volume':
                        data.volume = cursor.get_double(i);
                        break;
                    case 'position':
                        data.position = cursor.get_integer(i);
                        break;
                    default:
                }
            }
            this.#zaps.append(new Zap(data));
        }
    }

    /**
     * Retrieve a Zap from a UUID.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap to retrieve.
     * @returns {Zap} The retrieved Zap.
     */
    get({ uuid }) {
        return this.#findZap(uuid).position;
    }

    /**
     * Add a Zap.
     *
     * @param {object} params Parameter object.
     * @param {string} params.name Name of the Zap.
     * @param {string} params.collectionUuid UUID of the collection the Zap belongs to.
     * @param {string} params.uri URI of the audio file the Zap will play.
     * @param {Color} params.color Color of the Zap.
     * @param {boolean} params.loop If the Zap is looping.
     * @param {number} params.volume Volume of the Zap, between 0 and 1.
     * @returns {Zap} The newly added Zap.
     * @throws Throws an error if the file doesn't exist.
     */
    add({ name, collectionUuid, uri, color = Color.GRAY, loop = false, volume = 1 }) {
        globalThis.collectionManager.get({ uuid: collectionUuid });

        const originalFile = Gio.File.new_for_uri(uri);
        if (!originalFile.query_exists(null))
            throw new Error(`File '${uri}' does not exist.`);
        const uuid = GLib.uuid_string_random();
        const originalFileName = originalFile.get_basename();
        const extension = originalFileName.substring(originalFileName.lastIndexOf('.') + 1, originalFileName.length) || originalFileName;
        const file = ZAPS_DIR.get_child(`${uuid}.${extension}`);
        originalFile.copy(file, Gio.FileCopyFlags.NONE, null, null);

        const zap = new Zap({
            uuid,
            name,
            collectionUuid,
            file,
            color,
            loop,
            volume,
            position: this.#getTotalInCollection(collectionUuid),
        });

        const resource = Tracker.Resource.new(null);
        resource.set_uri('rdf:type', 'zap:Zap');
        resource.set_string('zap:uuid', zap.uuid);
        resource.set_string('zap:name', zap.name);
        resource.set_string('zap:collectionUuid', zap.collectionUuid);
        resource.set_string('zap:uri', zap.file.get_uri());
        resource.set_string('zap:color', zap.color.id);
        resource.set_boolean('zap:loop', zap.loop);
        resource.set_double('zap:volume', zap.volume);
        resource.set_int('zap:position', zap.position);
        globalThis.database.batch([resource]);

        this.#zaps.append(zap);
        this.emit('zap-added', zap.uuid);
        return zap;
    }

    /**
     * Remove a Zap.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     */
    remove({ uuid }) {
        const { zap, position } = this.#findZap(uuid);

        globalThis.database.update(
            `DELETE {
                ?zap a rdfs:Resource
            } WHERE {
                ?zap a zap:Zap;
                    zap:uuid ?uuid.
                FILTER (?uuid = "${Tracker.sparql_escape_string(uuid)}")
            }`
        );

        zap.file.delete(null);

        this.#zaps.remove(position);
        this.emit('zap-removed', uuid);
    }

    /**
     * Remove all the Zaps in the given collection.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the collection.
     */
    removeAllOfCollection({ uuid }) {
        for (let i = this.#zaps.get_n_items() - 1; i >= 0; i--) {
            const zap = this.#zaps.get_item(i);
            if (zap.collectionUuid === uuid)
                this.remove({ uuid: zap.uuid });
        }
    }

    /**
     * Rename a Zap.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {string} params.name New name.
     */
    rename({ uuid, name }) {
        this.#updateProperty(uuid, 'name', Tracker.sparql_escape_string(name));
    }

    /**
     * Change the collection a Zap belongs to.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {string} params.collectionUuid UUID of the new collection.
     */
    changeCollection({ uuid, collectionUuid }) {
        const { zap } = this.#findZap(uuid);
        if (zap.collectionUuid === collectionUuid)
            return;
        this.changePosition({ uuid, position: this.#getTotalInCollection(zap.collectionUuid) });
        this.#updateProperty(uuid, 'collectionUuid', Tracker.sparql_escape_string(collectionUuid));
        this.changePosition({ uuid, position: this.#getTotalInCollection(zap.collectionUuid) });
    }

    /**
     * Change the color of a Zap.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {Color} params.color New color.
     */
    changeColor({ uuid, color }) {
        const { zap, position } = this.#findZap(uuid);
        if (zap.color === color)
            return;
        globalThis.database.update(
            `DELETE {
                ?zap zap:color ?color
            } INSERT {
                ?zap zap:color "${color.id}"
            } WHERE {
                ?zap a zap:Zap;
                    zap:uuid ?uuid;
                    zap:color ?color.
                FILTER (?uuid = "${Tracker.sparql_escape_string(uuid)}")
            }`
        );
        zap.color = color;
        this.emit('zap-updated', zap.uuid);
        this.#zaps.emit('items-changed', position, 1, 1);
    }

    /**
     * Change if the Zap is looping or not.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {boolean} params.loop If the Zap will loop.
     */
    loop({ uuid, loop }) {
        this.#updateProperty(uuid, 'loop', loop);
    }

    /**
     * Change the volume of a Zap.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {number} params.volume New volume, between 0 and 1.
     */
    changeVolume({ uuid, volume }) {
        this.#updateProperty(uuid, 'volume', volume);
    }

    /**
     * Change the position of a Zap in its collection.
     *
     * @param {object} params Parameter object.
     * @param {string} params.uuid UUID of the Zap.
     * @param {number} params.position New position, an unsigned integer.
     */
    changePosition({ uuid, position }) {
        const { zap } = this.#findZap(uuid);
        if (zap.position === position)
            return;

        let desiredPosition = position;
        if (desiredPosition < 0)
            desiredPosition = 0;
        const totalInCollection = this.#getTotalInCollection(zap.collectionUuid);
        if (desiredPosition > totalInCollection)
            desiredPosition = totalInCollection - 1;

        const oldPosition = zap.position;
        const diff = oldPosition - desiredPosition;
        for (let i = 0; i < this.#zaps.get_n_items(); i++) {
            const z = this.#zaps.get_item(i);
            if (z === zap) {
                this.#updateProperty(uuid, 'position', desiredPosition);
                continue;
            }
            if (z.collectionUuid !== zap.collectionUuid)
                continue;
            if (diff < 0 && (z.position < oldPosition || z.position > desiredPosition))
                continue;
            if (diff > 0 && (z.position < desiredPosition || z.position > oldPosition))
                continue;
            this.#updateProperty(z.uuid, 'position', z.position + Math.sign(diff));
        }
    }

    /**
     * Convenience method to update the property of a Zap.
     *
     * @param {string} uuid UUID of the Zap.
     * @param {string} property Name of the property.
     * @param {*} value New value.
     * @throws Throws an error if no Zap has the given UUID.
     */
    #updateProperty(uuid, property, value) {
        const { zap, position } = this.#findZap(uuid);
        if (zap[property] === value)
            return;
        globalThis.database.update(
            `DELETE {
                ?zap zap:${property} ?${property}
            } INSERT {
                ?zap zap:${property} "${value}"
            } WHERE {
                ?zap a zap:Zap;
                    zap:uuid ?uuid;
                    zap:${property} ?${property}.
                FILTER (?uuid = "${Tracker.sparql_escape_string(uuid)}")
            }`
        );
        zap[property] = value;
        this.emit('zap-updated', zap.uuid);
        this.#zaps.emit('items-changed', position, 1, 1);
    }

    /**
     * @typedef {object} FindZapResult
     * @property {Zap} zap The found Zap.
     * @property {number} position Position in the store, an unsigned integer.
     */
    /**
     * Convenience method to find a Zap in the store.
     *
     * @param {string} uuid UUID of the Zap.
     * @returns {FindZapResult} Object containing the Zap and its position in the store.
     * @throws Throws an error if no Zap has the given UUID.
     */
    #findZap(uuid) {
        for (let i = 0; i < this.#zaps.get_n_items(); i++) {
            const zap = this.#zaps.get_item(i);
            if (zap.uuid === uuid) {
                return {
                    zap,
                    position: i,
                };
            }
        }
        throw new Error(`No Zap found for UUID ${uuid}`);
    }

    /**
     * Get the total number of Zaps in a collection.
     *
     * @param {string} uuid UUID of the collection.
     * @returns {number} Total number.
     */
    #getTotalInCollection(uuid) {
        let n = 0;
        for (let i = 0; i < this.#zaps.get_n_items(); i++) {
            if (this.#zaps.get_item(i).collectionUuid === uuid)
                n++;
        }
        return n;
    }

}
