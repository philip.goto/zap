// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { Collection } from '../classes/Collection.js';


/**
 * Menu button displaying the collections.
 */
export class CollectionsMenuButton extends Gtk.Widget {

    static {
        GObject.registerClass({
            GTypeName: 'ZapCollectionsMenuButton',
            CssName: 'collections-menubutton',
            Template: 'resource:///fr/romainvigier/zap/ui/CollectionsMenuButton.ui',
            Properties: {
                selectedCollection: GObject.ParamSpec.object('selected-collection', 'Selected collection', 'Selected collection', GObject.ParamFlags.READWRITE, Collection),
                collections: GObject.ParamSpec.object('collections', 'Collections', 'Collections', GObject.ParamFlags.READWRITE, Gio.ListStore),
            },
            InternalChildren: ['addCollectionNameEntry', 'button'],
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     * @param {?Collection} params.selectedCollection Selected collection.
     * @param {?Gio.ListStore<Collection>} params.collections Collections.
     */
    constructor({ selectedCollection = null, collections = null, ...params } = {}) {
        super(params);

        /** @type {?Collection} */
        this.selectedCollection = selectedCollection;
        /** @type {?Gio.ListStore<Collection>} */
        this.collections = collections;

        /** @type {Gtk.Entry} */
        this._addCollectionNameEntry = this._addCollectionNameEntry; // eslint-disable-line no-self-assign
        /** @type {Gtk.MenuButton} */
        this._button = this._button; // eslint-disable-line no-self-assign

        this.#setupActions();
    }

    /**
     * Setup the widget's actions.
     */
    #setupActions() {
        const actionGroup = new Gio.SimpleActionGroup();
        this.insert_action_group('menu', actionGroup);
        [
            {
                name: 'close',
                parameterType: null,
                callback: (action, params) => {
                    this.popdown();
                },
            },
        ].forEach(({ name, parameterType, callback }) => {
            const action = new Gio.SimpleAction({ name, parameterType });
            action.connect('activate', callback);
            actionGroup.insert(action);
        });
    }

    /**
     * Popup the menu.
     */
    popup() {
        this._button.popup();
    }

    /**
     * Popdown the menu.
     */
    popdown() {
        this._button.popdown();
    }

    /**
     * Callback when the add collection name entry is activated.
     *
     * @param {Gtk.Entry} entry Add collection name entry.
     */
    onAddCollectionNameEntryActivated(entry) {
        if (!entry.text)
            return;
        this.#addCollection();
    }

    /**
     * Callback when the add collection button is clicked.
     *
     * @param {Gtk.Button} button Add collection button.
     */
    onAddCollectionButtonClicked(button) {
        this.#addCollection();
    }

    /**
     * Validate if the add collection button is enabled.
     *
     * @param {Gtk.Button} button Add collection button.
     * @param {string} name Name.
     * @returns {boolean} Whether the values are valid.
     */
    validate(button, name) {
        return !!name;
    }

    /**
     * Add a new collection.
     */
    #addCollection() {
        const collection = globalThis.collectionManager.add({ name: this._addCollectionNameEntry.text });
        this.get_root().selectedCollection = collection;
        this.popdown();
        this._addCollectionNameEntry.text = '';
    }

}
