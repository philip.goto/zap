// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { Color } from '../enums/Color.js';


/**
 * Pill representing a color.
 */
export class ColorPill extends Gtk.Widget {

    /** @type {Gtk.Button} */
    #button;
    /** @type {Gtk.CssProvider} */
    #provider;

    static {
        GObject.registerClass({
            GTypeName: 'ZapColorPill',
            CssName: 'colorpill',
            Properties: {
                color: GObject.ParamSpec.jsobject('color', 'Color', 'Color', GObject.ParamFlags.READWRITE, Color.GRAY),
            },
            Signals: {
                clicked: {},
            },
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     * @param {Color} params.color Color.
     */
    constructor({ color = Color.GRAY, ...params } = {}) {
        super(params);

        /**
         * Color represented by the pill.
         *
         * @type {Color}
         */
        this.color = color;

        this.#setupButton();
        this.#setupLayout();
        this.#setupStyling();
        this.#syncCss();
        this.#syncTooltip();

        this.connect('notify::color', () => {
            this.#syncCss();
            this.#syncTooltip();
        });
    }

    /**
     * Setup the button.
     */
    #setupButton() {
        this.#button = new Gtk.Button();
        this.#button.connect('clicked', () => this.emit('clicked'));
        this.#button.set_parent(this);
    }

    /**
     * Setup the layout.
     */
    #setupLayout() {
        this.layoutManager = new Gtk.BinLayout();
        this.widthRequest = 32;
        this.heightRequest = 32;
    }

    /**
     * Setup the styling.
     */
    #setupStyling() {
        this.#provider = new Gtk.CssProvider();
        const context = this.#button.get_style_context();
        context.add_provider(this.#provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    /**
     * Synchronize the CSS colors with the color.
     */
    #syncCss() {
        const css = `
            @define-color pill_color_light ${this.color.rgba.light.to_string()};
            @define-color pill_color_dark ${this.color.rgba.dark.to_string()};
        `;
        this.#provider.load_from_data(css);
    }

    /** Synchronize the tooltip with the color. */
    #syncTooltip() {
        this.tooltipText = this.color.name;
    }

}
