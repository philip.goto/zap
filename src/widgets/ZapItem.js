// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Gdk from 'gi://Gdk';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { Zap } from '../classes/Zap.js';

import { Color } from '../enums/Color.js';

import { ProgressBar } from './ProgressBar.js';


/**
 * Widget displaying and allowing interaction with a Zap.
 */
export class ZapItem extends Gtk.Widget {

    /** @type {Gtk.CssProvider[]} */
    #providers = [];

    static {
        GObject.registerClass({
            GTypeName: 'ZapZapItem',
            CssName: 'zap-item',
            Template: 'resource:///fr/romainvigier/zap/ui/ZapItem.ui',
            Properties: {
                zap: GObject.ParamSpec.object('zap', 'Zap', 'Zap', GObject.ParamFlags.READWRITE, Zap),
                color: GObject.ParamSpec.jsobject('color', 'Color', 'Color', GObject.ParamFlags.READWRITE, Color.GRAY),
                playing: GObject.ParamSpec.boolean('playing', 'Playing', 'Playing', GObject.ParamFlags.READWRITE, false),
            },
            InternalChildren: ['editButton', 'fadeOutButton', 'loopButton', 'playButton', 'progressBar', 'stopButton', 'stopButtonRevealer'],
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     * @param {?Zap} params.zap Zap.
     * @param {Color} params.color Color.
     * @param {boolean} params.playing Playing state.
     */
    constructor({ zap = null, color = Color.GRAY, playing = false, ...params } = {}) {
        super(params);

        /** @type {?Zap} */
        this.zap = zap;
        /** @type {?Color} */
        this.color = color;
        /** @type {?boolean} */
        this.playing = playing;

        /** @type {Gtk.Button} */
        this._editButton = this._editButton; // eslint-disable-line no-self-assign
        /** @type {Gtk.Button} */
        this._fadeOutButton = this._fadeOutButton; // eslint-disable-line no-self-assign
        /** @type {Gtk.Button} */
        this._loopButton = this._loopButton; // eslint-disable-line no-self-assign
        /** @type {Gtk.Button} */
        this._playButton = this._playButton; // eslint-disable-line no-self-assign
        /** @type {ProgressBar} */
        this._progressBar = this._progressBar; // eslint-disable-line no-self-assign
        /** @type {Gtk.Button} */
        this._stopButton = this._stopButton; // eslint-disable-line no-self-assign
        /** @type {Gtk.Revealer} */
        this._stopButtonRevealer = this._stopButtonRevealer; // eslint-disable-line no-self-assign

        this.#setupStyling();
        this.#syncCss();
        this.#syncCssClass();

        this.connect('notify::color', () => this.#syncCss());
        this.connect('notify::playing', () => this.#syncCssClass());
    }

    /**
     * Setup styling.
     */
    #setupStyling() {
        [this._editButton, this._fadeOutButton, this._loopButton, this._playButton, this._progressBar, this._stopButton].forEach(widget => {
            const provider = new Gtk.CssProvider();
            // We need to access the style context of the button inside the edit menu button
            const context = widget === this._editButton ? widget.get_first_child().get_style_context() : widget.get_style_context();
            context.add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            this.#providers.push(provider);
        });
    }

    /**
     * Synchronize the CSS colors with the Zap's color.
     */
    #syncCss() {
        this.#providers.forEach(provider => {
            const css = `
                @define-color zap_color_light ${this.color.rgba.light.to_string()};
                @define-color zap_color_dark ${this.color.rgba.dark.to_string()};
            `;
            provider.load_from_data(css);
        });
    }

    /**
     * Synchronize CSS class names to the Zap's state.
     */
    #syncCssClass() {
        if (this._stopButtonRevealer.childRevealed || this.playing)
            this.add_css_class('playing');
        else
            this.remove_css_class('playing');
        // Object.values(Color).forEach(className => {
        //     if (className === Color.NONE)
        //         return;
        //     this.remove_css_class(className);
        // });
        // if (this.color !== Color.NONE)
        //     this.add_css_class(this.color);
    }

    /**
     * Callback when the stop button revealer changes.
     *
     * @param {Gtk.Revealer} revealer Revealer.
     */
    onStopButtonRevealChanged(revealer) {
        this.#syncCssClass();
    }

    /**
     * Callback when the stop button is clicked.
     *
     * @param {Gtk.Button} button Stop button.
     */
    onStopButtonClicked(button) {
        globalThis.player.stop();
    }

    /**
     * Callback when the fade out button is clicked.
     *
     * @param {Gtk.Button} button Fade out button.
     */
    onFadeOutButtonClicked(button) {
        globalThis.player.fadeOut();
    }

    /**
     * Callback when the play button is clicked.
     *
     * @param {Gtk.Button} button Play button.
     */
    onPlayButtonClicked(button) {
        if (!this.zap.file.query_exists(null)) {
            console.error('File does not exists');
            return;
        }
        globalThis.player.play(this.zap);
    }

    /**
     * Callback when the loop button is clicked.
     *
     * @param {Gtk.Button} button Loop button.
     */
    onLoopButtonToggled(button) {
        globalThis.zapManager.loop({
            uuid: this.zap.uuid,
            loop: button.active,
        });
    }

    /**
     * Callback when the drag source needs to be prepared.
     *
     * @param {Gtk.DragSource} source Drag source.
     * @param {number} x X coordinate.
     * @param {number} y Y coordinate.
     * @returns {Gdk.ContentProvider} Content provider.
     */
    onDragPrepare(source, x, y) {
        source.set_icon(new Gtk.WidgetPaintable({ widget: this }), x, y);
        const value = new GObject.Value();
        value.init_from_instance(this.zap);
        return Gdk.ContentProvider.new_for_value(value);
    }

    /**
     * Callback when the value of the drop changes.
     *
     * @param {Gtk.DropTarget} target Drop target.
     */
    onDropValueChanged(target) {
        // FIXME: value is not preloaded
        if (!target.value)
            return;
        const droppedZap = target.value;
        if (droppedZap === this.zap)
            target.reject();
        if (droppedZap.collectionUuid !== this.zap.collectionUuid)
            target.reject();
    }

    /**
     * Callback when a Zap is dropped.
     *
     * @param {Gtk.DropTarget} target Drop target.
     * @param {Zap} value Dropped Zap.
     * @param {number} x X coordinate.
     * @param {number} y Y coordinate.
     * @returns {boolean} Whether the drop is handled.
     */
    onDrop(target, value, x, y) {
        if (value === this.zap)
            return false;
        if (value.collectionUuid !== this.zap.collectionUuid)
            return false;
        globalThis.zapManager.changePosition({
            uuid: value.uuid,
            position: this.zap.position,
        });
        return true;
    }

}
