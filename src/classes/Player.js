// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gst from 'gi://Gst';
import GstAudio from 'gi://GstAudio';
import GstController from 'gi://GstController';

import { Zap } from './Zap.js';


/**
 * The Player plays Zaps.
 *
 * After constructing it, you have to `start()` it, and `destroy()` it on exit.
 *
 * Use the `play()` method to play a Zap.
 */
export class Player extends GObject.Object {

    /** @type {Gst.Bin} */
    #audioSink;
    /** @type {Gst.Element} */
    #playbin;
    /** @type {Gst.Element} */
    #volumeElement;
    /** @type {GstController.InterpolationControlSource} */
    #fadeControlSource;
    /** @type {Gst.Bus} */
    #bus;
    /** @type {?number} */
    #fadeTimeout = null;
    /** @type {?number} */
    #progressTimeout = null;
    /** @type {number[]} */
    #connections = [];

    static {
        GObject.registerClass({
            GTypeName: 'ZapPlayer',
            Properties: {
                zap: GObject.ParamSpec.object('zap', 'Zap', 'Zap', GObject.ParamFlags.READWRITE, Zap),
            },
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     * @param {Zap} params.zap The Zap to play.
     */
    constructor({ zap = null, ...params } = {}) {
        super(params);

        /**
         * The currently handled Zap.
         *
         * @type {Zap}
         */
        this.zap = zap;

        this.#setupAudioSink();
        this.#setupPlayBin();
        this.#setupFadeControlSource();

        this.#bus = this.#playbin.get_bus();
    }

    /**
     * Setup the audio sink.
     */
    #setupAudioSink() {
        this.#audioSink = new Gst.Bin({ name: 'audio-sink' });
        this.#volumeElement = Gst.ElementFactory.make('volume', 'fadevolume');
        const audioConvert = Gst.ElementFactory.make('audioconvert', 'audioconvert');
        const autoAudioSink = Gst.ElementFactory.make('autoaudiosink', 'autoaudiosink');

        this.#audioSink.add(this.#volumeElement);
        this.#audioSink.add(audioConvert);
        this.#audioSink.add(autoAudioSink);

        this.#volumeElement.link(audioConvert);
        audioConvert.link(autoAudioSink);

        const ghostPad = Gst.GhostPad.new('sink', this.#volumeElement.get_static_pad('sink'));
        ghostPad.set_active(true);
        this.#audioSink.add_pad(ghostPad);
    }

    /**
     * Setup the play bin.
     */
    #setupPlayBin() {
        this.#playbin = Gst.ElementFactory.make('playbin', 'playbin');

        this.#playbin.audioSink = this.#audioSink;
        this.#playbin.textSink = Gst.ElementFactory.make('fakesink', 'text-sink');
        this.#playbin.videoSink = Gst.ElementFactory.make('fakesink', 'video-sink');
    }

    /**
     * Setup the fade control source.
     */
    #setupFadeControlSource() {
        this.#fadeControlSource = new GstController.InterpolationControlSource({
            mode: GstController.InterpolationMode.LINEAR,
        });
        const fadeControlBinding = new GstController.DirectControlBinding({
            object: this.#volumeElement,
            name: 'volume',
            absolute: true,
            controlSource: this.#fadeControlSource,
        });
        this.#volumeElement.add_control_binding(fadeControlBinding);
    }

    /**
     * If the player is playing.
     *
     * @returns {boolean} Playing state.
     */
    get playing() {
        return this.zap ? this.zap.playing : false;
    }

    /**
     * Start the player.
     */
    start() {
        this.#bus.add_signal_watch();
        this.#bus.connect('message::eos', () => this.#onPlayEnded());
        this.#bus.connect('message::warning', (bus, message) => console.warn(message.parse_warning()[0].toString()));
        this.#bus.connect('message::error', (bus, message) => console.error(message.parse_error().toString()));

        // FIXME: Error when the signal is emitted, see https://gitlab.gnome.org/GNOME/gjs/-/issues/506
        // this.#playbin.connect('about-to-finish', () => this.#onAboutToFinish());
    }

    /**
     * Destroy the player.
     */
    destroy() {
        this.#stopUpdatingProgress();
        this.#bus.remove_signal_watch();
        this.#playbin.set_state(Gst.State.NULL);
    }

    /**
     * Play the given Zap.
     *
     * If the Zap is already playing, play it again from the beginning.
     *
     * @param {Zap} zap The Zap to play.
     */
    play(zap) {
        if (zap === this.zap && zap.playing) {
            this.#playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, 0);
            return;
        }
        this.stop();
        this.#setup(zap);
        this.zap.playing = true;
        this.#playbin.set_state(Gst.State.PLAYING);

        this.#updateProgress();
    }

    /**
     * Stop playing the currently playing Zap, if any.
     */
    stop() {
        this.#stopUpdatingProgress();
        this.#playbin.set_state(Gst.State.NULL);
        this.#fadeControlSource.unset_all();
        this.#volumeElement.volume = 1;
        if (this.#fadeTimeout)
            GLib.source_remove(this.#fadeTimeout);
        if (this.zap) {
            this.zap.playing = false;
            this.zap.progress = 0;
        }
    }

    /**
     * Fade out and stop the currently playing zap, if any.
     *
     * @param {number} length Length of the fade out, in seconds.
     */
    fadeOut(length = 1) {
        if (!this.zap || !this.zap.playing)
            return;
        const [positionOk, position] = this.#playbin.query_position(Gst.Format.TIME);
        this.#fadeControlSource.set(position, this.#volumeElement.volume);
        this.#fadeControlSource.set(position + length * Gst.SECOND, 0.0);
        this.#fadeTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, length * 1000, () => {
            this.stop();
            this.#fadeTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    /**
     * Setup the player with the given zap.
     *
     * @param {Zap} zap The Zap.
     */
    #setup(zap) {
        this.#disconnectFromZap();
        this.zap = zap;
        this.#connectToZap();
        this.#playbin.uri = zap.file.get_uri();
        this.#syncVolume();
    }

    /**
     * Connect to the zap signals.
     */
    #connectToZap() {
        this.#connections.push(this.zap.connect('notify::volume', zap => {
            this.#syncVolume();
        }));
    }

    /**
     * Disconnect from the zap signals.
     */
    #disconnectFromZap() {
        this.#connections.forEach(connection => this.zap.disconnect(connection));
        this.#connections = [];
    }

    /**
     * Start regularly updating the zap's progress.
     */
    #updateProgress() {
        this.#progressTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 10, () => {
            const [positionOk, position] = this.#playbin.query_position(Gst.Format.TIME);
            const [durationOk, duration] = this.#playbin.query_duration(Gst.Format.TIME);
            if (positionOk && durationOk)
                this.zap.progress = position / duration;
            return GLib.SOURCE_CONTINUE;
        });
    }

    /**
     * Stop updating the zap's progress.
     */
    #stopUpdatingProgress() {
        if (this.#progressTimeout) {
            GLib.source_remove(this.#progressTimeout);
            this.#progressTimeout = null;
        }
    }

    /**
     * Sync the volume element to the zap's volume.
     */
    #syncVolume() {
        const linearVolume = GstAudio.StreamVolume.convert_volume(GstAudio.StreamVolumeFormat.CUBIC, GstAudio.StreamVolumeFormat.LINEAR, this.zap.volume);
        this.#volumeElement.volume = linearVolume;
    }

    // #onAboutToFinish() {
    //     if (this.zap.loop)
    //         this.#playbin.set_property('uri', this.zap.file.get_uri());
    // }

    /**
     * Callback function when the Zap finishes playing.
     */
    #onPlayEnded() {
        if (this.zap.loop) {
            this.#playbin.set_state(Gst.State.NULL);
            this.#playbin.set_state(Gst.State.PLAYING);
        } else {
            this.stop();
        }
    }

}
