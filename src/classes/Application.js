// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';

import { Player } from './Player.js';

import { CollectionManager } from '../data/Collections.js';
import { ZapManager } from '../data/Zaps.js';

import { Database } from '../services/Database.js';
import { DBusService } from '../services/DBusService.js';

import { Window } from '../widgets/Window.js';


/** @type {CollectionManager} */
globalThis.collectionManager = null;
/** @type {Database} */
globalThis.database = null;
/** @type {Player} */
globalThis.player = null;
/** @type {Gio.Settings} */
globalThis.settings = null;
/** @type {ZapManager} */
globalThis.zapManager = null;


/**
 * The application.
 */
export class Application extends Adw.Application {

    dBusService;

    static {
        GObject.registerClass({
            GTypeName: 'ZapApplication',
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     */
    constructor({ ...params } = {}) {
        super({
            application_id: pkg.name,
            flags: Gio.ApplicationFlags.FLAGS_NONE,
            ...params,
        });
        // Translators: Application name, avoid translating it!
        GLib.set_application_name(_('Zap'));
        globalThis.settings = new Gio.Settings({ schemaId: this.applicationId });
        globalThis.player = new Player();
        globalThis.database = new Database();
        globalThis.collectionManager = new CollectionManager();
        globalThis.zapManager = new ZapManager();
        this.dBusService = new DBusService();
    }

    /**
     * Startup virtual method.
     */
    vfunc_startup() {
        super.vfunc_startup();
        globalThis.player.start();
        globalThis.database.connect();
        globalThis.collectionManager.restore();
        globalThis.zapManager.restore();
        this.dBusService.start();
        this.#setupActions();
        this.#setupAccelerators();
    }

    /**
     * Activate virtual method.
     */
    vfunc_activate() {
        this.newWindow();
    }

    /**
     * Shutdown virtual method.
     */
    vfunc_shutdown() {
        this.dBusService.stop();
        globalThis.player.destroy();
        globalThis.database.disconnect();
        super.vfunc_shutdown();
    }

    /**
     * Open a new window.
     */
    newWindow() {
        const window = new Window({ application: this });
        window.present();
    }

    /**
     * Setup the actions.
     */
    #setupActions() {
        [
            {
                name: 'quit',
                parameterType: null,
                callback: (action, params) => {
                    this.quit();
                },
            },
            {
                name: 'new-window',
                parameterType: null,
                callback: (action, params) => {
                    this.newWindow();
                },
            },
        ].forEach(({ name, parameterType, callback }) => {
            const action = new Gio.SimpleAction({ name, parameterType });
            action.connect('activate', callback);
            this.add_action(action);
        });
    }

    /**
     * Setup the accelerators.
     */
    #setupAccelerators() {
        this.set_accels_for_action('app.new-window', ['<Control>n']);
        this.set_accels_for_action('app.quit', ['<Control>q']);
        this.set_accels_for_action('win.open-add-zap-popup', ['F2']);
        this.set_accels_for_action('win.open-collections-popover', ['F3']);
        this.set_accels_for_action('window.close', ['<Control>w']);
    }

}
