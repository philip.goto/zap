# SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
root: true
env:
  browser: true
  es2022: true
parserOptions:
  ecmaVersion: latest
  sourceType: module
plugins:
  - jsdoc
extends:
 - 'eslint:recommended'
 - 'plugin:jsdoc/recommended'
globals:
  _: readonly
  imports: readonly
  log: readonly
  logError: readonly
  pkg: readonly
  print: readonly
  printerr: readonly
rules:
  array-bracket-newline:
    - error
    - consistent
  array-callback-return: error
  arrow-parens:
    - error
    - as-needed
  arrow-spacing: error
  block-spacing: error
  brace-style: error
  camelcase:
    - error
    - properties: never
      allow: [^vfunc_]
  comma-dangle:
    - error
    - arrays: always-multiline
      objects: always-multiline
  comma-spacing: error
  comma-style: error
  computed-property-spacing: error
  consistent-return: error
  curly:
    - error
    - multi-or-nest
    - consistent
  default-case: error
  default-param-last: error
  dot-location:
    - error
    - property
  dot-notation: error
  eol-last: error
  eqeqeq: error
  func-call-spacing: error
  func-name-matching: error
  func-style:
    - error
    - declaration
    - allowArrowFunctions: true
  grouped-accessor-pairs: error
  indent:
    - error
    - 4
    - SwitchCase: 1
  key-spacing: error
  keyword-spacing: error
  linebreak-style: error
  lines-between-class-members:
    - error
    - always
    - exceptAfterSingleLine: true
  max-nested-callbacks:
    - error
    - max: 3
  max-statements-per-line: error
  new-cap: error
  new-parens: error
  no-array-constructor: error
  no-await-in-loop: error
  no-caller: error
  no-constant-condition:
    - error
    - checkLoops: false
  no-constructor-return: error
  no-div-regex: error
  no-duplicate-imports: error
  no-empty:
    - error
    - allowEmptyCatch: true
  no-extend-native: error
  no-extra-bind: error
  no-extra-parens:
    - error
    - all
    - conditionalAssign: false
      nestedBinaryExpressions: false
      returnAssign: false
  no-global-assign: error
  no-implicit-coercion:
    - error
    - allow:
      - '!!'
  no-invalid-this: error
  no-iterator: error
  no-implicit-globals: error
  no-label-var: error
  no-lone-blocks: error
  no-lonely-if: error
  no-loop-func: error
  no-nested-ternary: error
  no-new-object: error
  no-new-wrappers: error
  no-octal-escape: error
  no-param-reassign: error
  no-promise-executor-return: error
  no-proto: error
  no-restricted-globals:
    - error
    - name: window
      message: Use globalThis instead.
    - name: imports
      message: Use ESM imports instead.
  no-return-assign: error
  no-return-await: error
  no-self-compare: error
  no-shadow: error
  no-shadow-restricted-names: error
  no-spaced-func: error
  no-tabs: error
  no-template-curly-in-string: error
  no-throw-literal: error
  no-trailing-spaces: error
  no-undef-init: error
  no-unmodified-loop-condition: error
  no-unneeded-ternary: error
  no-unreachable-loop: error
  no-unused-expressions: error
  no-unused-private-class-members: error
  no-unused-vars:
    - error
    - args: none
      destructuredArrayIgnorePattern: .+
  no-use-before-define: error
  no-useless-call: error
  no-useless-computed-key: error
  no-useless-concat: error
  no-useless-constructor: error
  no-useless-rename: error
  no-useless-return: error
  no-var: error
  no-whitespace-before-property: error
  no-with: error
  nonblock-statement-body-position:
    - error
    - below
  object-curly-newline:
    - error
    - consistent: true
      multiline: true
  object-curly-spacing:
    - error
    - always
  object-shorthand: error
  one-var:
    - error
    - never
  operator-assignment: error
  operator-linebreak: error
  padded-blocks:
    - error
    - blocks: never
      classes: always
      switches: never
  prefer-arrow-callback: error
  prefer-const: error
  prefer-destructuring: error
  prefer-numeric-literals: error
  prefer-promise-reject-errors: error
  prefer-rest-params: error
  prefer-spread: error
  prefer-template: error
  quotes:
    - error
    - single
    - avoidEscape: true
  quote-props:
    - error
    - consistent-as-needed
  require-atomic-updates: error
  require-await: error
  rest-spread-spacing: error
  semi: error
  semi-spacing: error
  semi-style: error
  space-before-blocks: error
  space-before-function-paren:
    - error
    - named: never
      anonymous: always
      asyncArrow: always
  space-in-parens: error
  space-infix-ops: error
  space-unary-ops: error
  spaced-comment: error
  switch-colon-spacing: error
  symbol-description: error
  template-curly-spacing: error
  template-tag-spacing: error
  unicode-bom: error
  wrap-iife:
    - error
    - inside
  yield-star-spacing: error
  yoda: error
  # JSDoc
  jsdoc/check-indentation: error
  jsdoc/require-description:
    - error
    - checkConstructors: false
  jsdoc/require-description-complete-sentence: error
  jsdoc/require-jsdoc:
    - error
    - require:
        ClassDeclaration: true
        MethodDefinition: true
  jsdoc/require-param: error
  jsdoc/require-throws: error
