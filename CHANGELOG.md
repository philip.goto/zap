<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Changelog


## 1.1.0 - 2022-10-06

### Added

- Ability to stop playing with a fade out
- German translation by @gastornis

### Fixed

- The default collection was not created
- Wrong application name in the About window


## 1.0.0 - 2022-09-30

Initial release.
